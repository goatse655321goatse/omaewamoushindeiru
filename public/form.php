<link rel='stylesheet' href='style.css' />
<div class="contentAll">
  <form action="" method="POST">
    <label><input name="fio" id="name" placeholder="Имя" /></label>
    <label><input type="email" id="mail" name="email" placeholder="E-mail"></label>

    <select name="date" id="data">
       <!--<option value="0" selected disabled>Год рождения</option>
       <option value="1980-1990">1980-1990</option>
			 <option value="1991-2001">1991-2001</option>
			 <option value="2002-2012">2002-2012</option>-->
        <?php for($i = 1900; $i < 2020; $i++) { ?>
          <option value="<?php print $i; ?>"><?= $i; ?></option>
        <?php } ?>
		</select>
    
    <label for="sex"><p>Пол: </p></label>
    <div>
      <input type="radio" id="Male"
      name="sex" value="M" checked>
      <label for="Male">Мужской</label>

      <input type="radio" id="Female"
      name="sex" value="F">
      <label for="Female">Женский</label>
    </div>

    <label for="limbs"><p>Количество конечностей: </p></label>
    <div>
      <input type="radio" id="Zero"
      name="limbs" value="0" disabled checked>
      <label for="Zero">Ни одной</label>

      <input type="radio" id="One"
      name="limbs" value="4">
      <label for="One">Четыре</label>

      <input type="radio" id="Two"
      name="limbs" value="2">
      <label for="Two">Две</label>

      <input type="radio" id="Three"
      name="limbs" value="6">
      <label for="Three">Шесть</label>
    </div>
    <br>

    <div  id="Abilit">
      <select  id="Abilit" name="abilities[]" multiple>
        <option value="0" selected disabled>Сверхспособность</option>
        <option value="Immortality">Бессмертие</option>
        <option value="Through_the_walls">Прохождение сквозь стены</option>
        <option value="Levitation">Левитация</option>
      </select>
    </div> 
      <!--<select name="abilities[]"  id="Abilit" multiple>
  <?php 
  foreach ($ability_labels as $key => $value) {
  ?>
    <option value="<? $key; ?>"><?= $value; ?></option>
  <?php } ?>-->


    <div><textarea name="biography" id="Biograph" rows="4" cols="50" placeholder="Биография"></textarea></div>

    <label for="Agree">На всё согласен: </label>
    <input type="checkbox" id="Agree" name="agree" value="Agree">
  
    <div><input type="submit" value="Отправить" id="btnsubmit"/></div>
  </form>
</div>
