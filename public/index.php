<?php
/**
 * Интернет-программирование. Задача 8.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

$ability_labels = ['Immortality' => 'Бессмертие', 'Through_the_walls' => 'Полет', 'Levitation' => 'Леха', ];

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
  print('Недопустимые символы в имени.<br/>');
  $errors = TRUE;
}


if (empty($_POST['email'])) {
  print('Заполните адрес электронной почты.<br/>');
  $errors = TRUE;
}
else if(!preg_match('/^.+@.+\..+$/u', $_POST['email'])){
  print('Некорректный адрес.<br/>');
  $errors = TRUE;
}

if (empty($_POST['date'])) {
  print('Выберите дату.<br/>');
  $errors = TRUE;
}
else {
  $date = $_POST['date'];
  if (!(is_numeric($date) && intval($date) >= 1900 && intval($date) <= 2020)) {
    print('Некорректный год.<br/>');
    $errors = TRUE;
  }
}

$ability_data = array_keys($ability_labels);
if (empty($_POST['abilities'])) {
    print('Выберите способность.<br/>');
    $errors = TRUE;
}
else{
  $abilities = $_POST['abilities'];
  foreach ($abilities as $ability) {
    if (!in_array($ability, $ability_data)) {
      print('Плохая способность!<br/>');
      $errors = TRUE;
    }
  }
}
$ability_insert = [];
foreach ($ability_data as $ability) {
  $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}


if($_POST['biography'] == "") {
  print('Расскажите о себе.<br/>');
  $errors = TRUE;
}

if(empty($_POST['agree'])) {
  print('Согласитесь, иначе форма не отправится.<br/>');
  $errors = TRUE;
}

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20366';
$pass = '5918475';
$db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  //$stmt = $db->prepare("INSERT INTO application SET name = ?");
  //$stmt -> execute(array($_POST['fio']));
  $stmt = $db->prepare("INSERT INTO application (name, email, sex, biography, birth_year, limbs, ability, agree) VALUES (:fio, :email, :sex, :biography, :date, :limbs, :ability, :agree)");
  $fio = $_POST['fio'];
  $stmt->bindParam(':fio', $fio, PDO::PARAM_STR);
  $email = $_POST['email'];
  $stmt->bindParam(':email', $email, PDO::PARAM_STR);
  $sex = $_POST['sex'];
  $stmt->bindParam(':sex', $sex, PDO::PARAM_STR);
  $biography = $_POST['biography'];
  $stmt->bindParam(':biography', $biography, PDO::PARAM_STR);
  $date = $_POST['date'];
  $stmt->bindParam(':date', $date, PDO::PARAM_STR);
  $limbs = $_POST['limbs'];
  $stmt->bindParam(':limbs', $limbs, PDO::PARAM_INT);
  
  $ability = $_POST['abilities'];
  $ability = implode(' ',$_POST['abilities']);
  $stmt->bindParam(':ability', $ability, PDO::PARAM_STR);
  $agree = $_POST['agree'];
  $stmt->bindParam(':agree', $agree, PDO::PARAM_STR);
  $stmt->execute();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
